### Week 2
#### 5th June to 12th June

It was an eventful week and I would say I learnt a few things.
I finally got the import CSS working and I can officially say the entire import/export CSS works flawlessly.

However, after a detailed discussion on the inkscape group chat realised that the user base for this feature  might be small and there can be scope for improvement.

I appreciated the feedback and introspected on how i could improve, And reached to the conclusion that a more detailed discussion and research before implementation of features would help.

After discussion with a few inkscape developers, I did decide on a few features that should be implemented, primary among them was the ability to edit a Selector without having to apply it to an object first. 

Adam.Bellis was nice enough to make some mockups which can be used as inspiration for implementing it and that will be what I will be working on the coming week. 

Will have to examine the codebase further to see how to implement it exactly. 

This is a relatively small blog since past week was filled with a lot of research 

I'll start implementation of seeing CSS of selectors in the coming week.


Till then,
**Arrivederci**
