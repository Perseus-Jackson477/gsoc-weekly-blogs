### Week 4
#### 19th June to 26th June

This was a pretty productive week in terms of work. 


I started working on the inactive CSS feature. 


I have almost completed it with a very small bug remaining to be fixed (Even after commenting out the CSS, the checkbox of that feature is still active). 


The working is simple enough. I search for the CSS name in the stylesheet and append /* and */ at the ends making the attribute inactive. 


I also changed the code which used to delete commented out CSS and parseStyle function. 


All seems to work fine except the small bug that i hope to fix by today or tomorrow. 


After this I'll recieve the feedback on the working of the feature, make necessary improvement and start working on the subsequent features after sufficient feedback and review from the devel team.


