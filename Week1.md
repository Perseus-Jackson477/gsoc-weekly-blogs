### Week 1
##### 29th May, 2023 to 4th June 2023

It was quite an eventful week and I feel like I learnt a lot as well as accomplished what targets I had planned.  

My first target was to allow users to export and import CSS files. 
Now obviously for import, there needs to be a decided syntax which I can parse easily. 
Thus I decided to first implement the export CSS feature 

Initially, I figured out how traditionally the exporting documents as SVG/PNG etc. works and then taking inspiration from that started working on a similar dialog box for exporting CSS.  

I created the inherited class of CSSSaveDialog with base class as FileDialog with all the relevant functions that I will need for exporting CSS.  

Apart from that I also implemented the _ExportCSS function which enables me to write the CSS of the selected objects to a file which I can export. 

I finally have the POC of export CSS ready. (Also shared on the inkscape develchat)
![exportcss](./assets/exportCSS.png)


The only problem I am currently facing is an error where I can't change the directory of the file I am saving (i.e. it's being saved in root even if I change the directory). This is but a minor inconvenience and i hope to figure it out in a day, failing to which, I would approach other inkscape developers for advice. 

It might be possible that my code is not production quality just yet and I would need to rewrite/change a few things, whhich I am readily available to do too. 

My plan of action is for each feature to create a proof of concept, because I find it easily achievable and after successfully doing that, making the code production level is easy. 

The target for the coming week for me would be to finish implementation for the import CSS feature and Solve the absolute path problem.After which I can easily start to make the code production quality, solve the minors issues here and there.  

I feel so far I am able to stick to the timeline I mentioned in the proposal.


I will report with another blog on 11th June, 2023 with more progress. 

Till then,
**Arrivederci**

