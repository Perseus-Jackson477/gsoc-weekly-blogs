### Week 3
#### 12th June to 19th June

I worked on a proposal to work on a selector editor feature this week and started working on it.


However after further consultation from the inkscape developers I realised that considering the ongoing GTK4 migration of inkscape, we might have to refactor most of the code, Hence it was decided to scrap the idea. 

There was a lot of confusion regarding how to proceed, thus I decided to have a meet with my mentor on what to do moving forward. 


That helped a lot and provided me with a lot of clarity. 


Not a lot to report this week since I was also travelling. In the coming week i'll be working on inactive CSS feature. 
